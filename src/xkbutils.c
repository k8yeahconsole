/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "xkbutils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>


////////////////////////////////////////////////////////////////////////////////
int activeLayout (Display *dpy) {
  XkbStateRec state;
  memset(&state, 0, sizeof(state));
  XkbGetState(dpy, XkbUseCoreKbd, &state);
  return (int)state.group;
}


void setActiveLayout (Display *dpy, int group) {
  XkbStateRec state;
  memset(&state, 0, sizeof(state));
  XkbLockGroup(dpy, XkbUseCoreKbd, group);
  // the following call is necessary
  XkbGetState(dpy, XkbUseCoreKbd, &state);
}


int layoutNames (Display *dpy, char *names[]) {
  XkbDescRec desc;
  int cnt;
  memset(&desc, 0, sizeof(desc));
  desc.device_spec = XkbUseCoreKbd;
  XkbGetControls(dpy, XkbGroupsWrapMask, &desc);
  XkbGetNames(dpy, XkbGroupNamesMask, &desc);
  cnt = (int)desc.ctrls->num_groups;
  XGetAtomNames(dpy, desc.names->groups, cnt, names);
  XkbFreeControls(&desc, XkbGroupsWrapMask, True);
  XkbFreeNames(&desc, XkbGroupNamesMask, True);
  return cnt;
}


void freeLayoutNames (char **names, int cnt) {
  for (; cnt > 0; ++names, --cnt) if (*names) { XFree(*names); *names = NULL; }
}


void printLayouts (Display *dpy) {
  char *names[XkbNumKbdGroups+1];
  int cnt = layoutNames(dpy, names);
  int active = activeLayout(dpy);
  for (int f = 0; f < cnt; ++f) printf("%d: %s%s\n", f, names[f], f==active?" (active)":"");
  freeLayoutNames(names, cnt);
}


int layoutCount (Display *dpy) {
  XkbDescRec desc[1];
  memset(desc, 0, sizeof(desc));
  desc->device_spec = XkbUseCoreKbd;
  XkbGetControls(dpy, XkbGroupsWrapMask, desc);
  return (int)desc->ctrls->num_groups;
}
