/*************************************************************************/
/*  _____________                                                        */
/* < yeahconsole >                                                       */
/*  -------------                                                        */
/*         \   ^__^                                                      */
/*          \  (oo)\_______                                              */
/*             (__)\       )\/\                                          */
/*                 ||----w |                                             */
/*                 ||     ||                                             */
/*                                                                       */
/*  Copyright (C) knorke                                                 */
/*  Heavily modified by Ketmar // Invisible Vector                       */
/*                                                                       */
/*  This program is free software; you can redistribute it and/or modify */
/*  it under the terms of the GNU General Public License as published by */
/*  the Free Software Foundation; either version 2 of the License, or    */
/*  (at your option) any later version.                                  */
/*                                                                       */
/*  This program is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/*  GNU General Public License for more details.                         */
/*                                                                       */
/*  You should have received a copy of the GNU General Public License    */
/*  along with this program; if not, write to the Free Software          */
/*  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.            */
/*************************************************************************/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
#include <X11/XKBlib.h>

#include "xkbutils.h"


#define UP    (0)
#define DOWN  (-height-optHandleWidth)


#define TRANSPARENCY_HACK  do { \
  XResizeWindow(dpy, termwin, optWidth, height+1); \
  XResizeWindow(dpy, termwin, optWidth, height); \
} while (0)


// xmodmap -pm
enum {
  Mod_Ctrl = ControlMask,
  Mod_Shift = ShiftMask,
  Mod_Alt = Mod1Mask,
  Mod_NumLock = Mod2Mask,
  Mod_AltGr = Mod3Mask,
  Mod_Hyper = Mod4Mask,
};


typedef int ybool;

typedef struct {
  KeySym ksym;
  unsigned long mods;
} KeyBind;


static Display *dpy;
static Window root;
static Window win;
static Window termwin = None;
static const char *progname;
static char command[4096];
static int revert_to;
static int screen;
static int height;
static int optX = 0;
static int optWidth = -1;
static int optHeight = 38;
static int optSlideDelay = 20;
static int optHandleWidth = 3;
static int optRestart = 0;
static int optMouseDeactivate = 0;
static const char *optColor = NULL;
static const char *optTermCmd = NULL;
static KeyBind optKeyToggle = {.ksym=NoSymbol};
static KeyBind optKeyRestartOnce = {.ksym=NoSymbol};
static KeyBind optKeySmaller = {.ksym=NoSymbol};
static KeyBind optKeyBigger = {.ksym=NoSymbol};
static KeyBind optKeyFullScreen = {.ksym=NoSymbol};
static KeyBind optKeyMouseDeactivate = {.ksym=NoSymbol};
static Cursor cursor;
static int resizeInc;
static int optDaemonize = 1;

static int reset_restart = 0;
static int old_restart = 0;
static int firstTimeH = 1;

static int was_x11_error = 0;

static int hidden = 1;
static int fullscreen = 0;
static int old_height = -1;
static Window lastFocused, currentFocused;
static int prevKBGroup = -1, prevMyKBGroup = 0;


// ////////////////////////////////////////////////////////////////////////// //
enum {
  otInt,
  otStr,
  otKey
};

typedef struct {
  int type;
  const char *name;
  const char *defs;
  int requiredkey;
  union {
    void *optV;
    int *optI;
    char **optS;
    KeyBind *optKS;
  } uopt;
} Option;

static Option options[] = {
  {.type=otInt, .name="screen-width",         .uopt={&optWidth}},
  {.type=otInt, .name="handle-width",         .uopt={&optHandleWidth}},
  {.type=otStr, .name="handle-color",         .defs="grey70", .uopt={&optColor}},
  {.type=otInt, .name="console-height",       .uopt={&optHeight}},
  {.type=otInt, .name="x-offset",             .uopt={&optX}},
  {.type=otInt, .name="ani-delay",            .uopt={&optSlideDelay}},
  {.type=otInt, .name="restart",              .uopt={&optRestart}},
  {.type=otStr, .name="term-cmd",             .defs="yterm -into <wid>", .uopt={&optTermCmd}},
  {.type=otInt, .name="mouse-deactivate",     .uopt={&optMouseDeactivate}},
  {.type=otKey, .name="key-toggle",           .defs="H-grave", .uopt={&optKeyToggle}, .requiredkey=1},
  {.type=otKey, .name="key-restart-once",     .defs="M-C-grave", .uopt={&optKeyRestartOnce}},
  {.type=otKey, .name="key-smaller",          .uopt={&optKeySmaller}},
  {.type=otKey, .name="key-bigger",           .uopt={&optKeyBigger}},
  {.type=otKey, .name="key-full",             .uopt={&optKeyFullScreen}},
  {.type=otKey, .name="key-mouse-deactivate", .uopt={&optKeyMouseDeactivate}},
  {0}
};


//==========================================================================
//
//  handle_xerror
//
//==========================================================================
static int handle_xerror (Display *display, XErrorEvent *e) {
  /* this function does nothing, we receive xerrors when the lastFocused window gets lost... */
  fprintf(stderr, "%d  XError caught\n", e->error_code);
  was_x11_error = 1;
  return 0;
}


//==========================================================================
//
//  buildTermCommand
//
//==========================================================================
static void buildTermCommand (int argc, char *argv[]) {
  char buf[32];
  char *pos = command;
  *pos = 0;
  const char *src = optTermCmd;
  ybool was_wid = 0;
  while (*src) {
    if (src[0] == '<' && src[1] == 'w' && src[2] == 'i' &&
        src[3] == 'd' && src[4] == '>')
    {
      snprintf(buf, sizeof(buf), "%d", (int)win);
      if ((ptrdiff_t)(command + sizeof(command) - pos) < (ptrdiff_t)(strlen(buf) + 2)) {
        fprintf(stderr, "FATAL: command line too big (wid)!\n");
        exit(1);
      }
      strcpy(pos, buf);
      pos += strlen(pos);
      src += 5;
      was_wid = 1;
    } else if (src[0] == '<' && src[1] == 'n' && src[2] == 'a' &&
               src[3] == 'm' && src[4] == 'e' && src[5] == '>')
    {
      if ((ptrdiff_t)(command + sizeof(command) - pos) < (ptrdiff_t)(strlen(progname) + 2)) {
        fprintf(stderr, "FATAL: command line too big (wid)!\n");
        exit(1);
      }
      strcpy(pos, progname);
      pos += strlen(pos);
      src += 6;
    } else {
      if (pos == command + sizeof(command)) {
        fprintf(stderr, "FATAL: command line too big!\n");
        exit(1);
      }
      *pos = *src;
      pos += 1; src += 1;
    }
  }
  if ((ptrdiff_t)(command + sizeof(command) - pos) < 4) {
    fprintf(stderr, "FATAL: command line too big (fin)!\n");
    exit(1);
  }
  strcat(pos, " &");
  if (!was_wid) {
    fprintf(stderr, "FATAL: no '<wid>' in command line!\n");
    exit(1);
  }
  /*
  if (strstr(optTerm, "sterm")) pos += sprintf(pos, "%s -into %d ", optTerm, (int)win);
  else if (strstr(optTerm, "yterm")) pos += sprintf(pos, "%s -embed %d -color mono ", optTerm, (int)win);
  else if (strstr(optTerm, "urxvt")) pos += sprintf(pos, "%s -b 0 -embed %d -name %s ", optTerm, (int)win, progname);
  else if (strstr(optTerm, "mrxvt")) pos += sprintf(pos, "%s -into %d -aht -bl -name %s ", optTerm, (int)win, progname);
  //else pos += sprintf(pos, "%s -b 0 -into %d -name %s ", optTerm, (int)win, progname);
  else pos += sprintf(pos, "%s -into %d -name %s ", optTerm, (int)win, progname);
  for (int f = 1; f < argc; ++f) pos += sprintf(pos, "%s ", argv[f]);
  if (optTermCmd != NULL) pos += sprintf(pos, "%s ", optTermCmd);
  strcat(pos, "&");
  */
}


//==========================================================================
//
//  runTerm
//
//==========================================================================
static int runTerm (int move) {
  XEvent ev;
  long dummy;
  XSizeHints *size;
  firstTimeH = 1;
  termwin = None;
  if (reset_restart) {
    reset_restart = 0;
    optRestart = old_restart;
  } else {
    if (WEXITSTATUS(system(command))) return -1;
  }
  for (;;) {
    XMaskEvent(dpy, SubstructureNotifyMask, &ev);
    if (ev.type == CreateNotify || ev.type == MapNotify) {
      termwin = ev.xcreatewindow.window;
      break;
    }
  }
  XSetWindowBorderWidth(dpy, termwin, 0);
  size = XAllocSizeHints();
  XGetWMNormalHints(dpy, termwin, size, &dummy);
  #if 0
  resizeInc = size->height_inc;
  #else
  resizeInc = 20;
  #endif
  if (move) {
    height = resizeInc*optHeight;
    if (size->base_height > 0) {
      int tbh = size->base_height-(size->base_height/resizeInc)*resizeInc; // tabbar height
      height += tbh;
    }
  }
  XFree(size);
  #if 0
  fprintf(stderr, "RUNTERM: resizeInc: %d\n", resizeInc);
  fprintf(stderr, "RUNTERM: height=%d\n", height);
  fprintf(stderr, "RUNTERM: base_height=%d\n", size->base_height);
  fprintf(stderr, "RUNTERM: hh=%d\n", size->height);
  #endif
  XResizeWindow(dpy, win, optWidth, height+optHandleWidth);
  if (termwin != None) XResizeWindow(dpy, termwin, optWidth, height);
  if (move) XMoveWindow(dpy, win, optX, -(height+optHandleWidth));
  return 0;
}


//==========================================================================
//
//  createWindow
//
//==========================================================================
static void createWindow (void) {
  XSetWindowAttributes attrib;
  XColor color;
  XColor dummycolor;
  attrib.override_redirect = True;
  attrib.background_pixel = BlackPixel(dpy, screen);
  win = XCreateWindow(
    dpy, root,
    optX, -200, optWidth, 200,
    0, CopyFromParent, InputOutput, CopyFromParent,
    CWOverrideRedirect|CWBackPixel, &attrib);
  XSelectInput(
    dpy, win,
    SubstructureNotifyMask|EnterWindowMask|LeaveWindowMask|KeyPressMask|ButtonPressMask|ButtonReleaseMask|FocusChangeMask);
  XAllocNamedColor(dpy, DefaultColormap(dpy, screen), optColor, &color, &dummycolor);
  XSetWindowBackground(dpy, win, color.pixel);
  XDefineCursor(dpy, win, cursor);
  {
    XClassHint class = {
      .res_name = "yeahconsole",
      .res_class = "yeahconsole",
    };
    XWMHints wm = { .flags = 0 };
    XSizeHints *size_hints = XAllocSizeHints();
    size_hints->flags = 0;
    XSetWMProperties(dpy, win, NULL, NULL, NULL, 0, size_hints, &wm, &class);
    XFree(size_hints);
  }
  XMapWindow(dpy, win);
}


//==========================================================================
//
//  resize
//
//==========================================================================
static void resize (void) {
  XEvent ev;
  if (!XGrabPointer(dpy, root, False, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, GrabModeAsync, GrabModeAsync, None, cursor, CurrentTime) == GrabSuccess) return;
  for (;;) {
    XMaskEvent(dpy, ButtonPressMask | ButtonReleaseMask | PointerMotionHintMask, &ev);
    switch (ev.type) {
      case MotionNotify:
        if (ev.xmotion.y >= resizeInc) {
          height = ev.xmotion.y-ev.xmotion.y%resizeInc;
          if (termwin != None) XResizeWindow(dpy, termwin, optWidth, height);
          XResizeWindow(dpy, win, optWidth, height+optHandleWidth);
          XSync(dpy, False);
        } else {
          XUngrabPointer(dpy, CurrentTime); /*k8: ???*/
        }
        break;
      case ButtonRelease:
        XUngrabPointer(dpy, CurrentTime);
        return;
    }
  }
}


//==========================================================================
//
//  grabKey
//
//==========================================================================
static ybool grabKey (KeyBind *kbind, const char *opt) {
  kbind->ksym = NoSymbol;
  kbind->mods = 0;
  const char *origopt = opt;
  if (opt) {
    while (*opt > 0 && *opt <= 32) opt += 1;
    while (*opt) {
      if (opt[0] == 'C' && opt[1] == '-') {
        kbind->mods |= ControlMask;
        opt += 2;
      } else if (opt[0] == 'M' && opt[1] == '-') {
        kbind->mods = Mod1Mask;
        opt += 2;
      } else if (opt[0] == 'H' && opt[1] == '-') {
        kbind->mods = Mod4Mask;
        opt += 2;
      } else if (opt[0] == 'S' && opt[1] == '-') {
        kbind->mods = ShiftMask;
        opt += 2;
      } else {
        break;
      }
    }
    //while (*opt > 0 && *opt <= 32) opt += 1;
    if (opt[0]) kbind->ksym = XStringToKeysym(opt);
  }
  if (kbind->ksym != NoSymbol) {
    XSync(dpy, 0);
    XGrabKey(dpy, XKeysymToKeycode(dpy, kbind->ksym), kbind->mods, root, True, GrabModeAsync, GrabModeAsync);
    XSync(dpy, 0);
    if (was_x11_error) {
      fprintf(stderr, "ERROR: cannot grab '%s'!\n", origopt);
      exit(1);
    }
    XGrabKey(dpy, XKeysymToKeycode(dpy, kbind->ksym), LockMask | kbind->mods, root, True, GrabModeAsync, GrabModeAsync);
    XSync(dpy, 0);
    if (was_x11_error) {
      fprintf(stderr, "ERROR: cannot grab release of '%s'!\n", origopt);
      exit(1);
    }
    /*
    if (numlockmask) {
      XGrabKey(dpy, XKeysymToKeycode(dpy, keysym), numlockmask | modmask, root, True, GrabModeAsync, GrabModeAsync);
      XGrabKey(dpy, XKeysymToKeycode(dpy, keysym), numlockmask | LockMask | modmask, root, True, GrabModeAsync, GrabModeAsync);
    }
    */
  }
  return (kbind->ksym != NoSymbol);
}


//==========================================================================
//
//  showOptions
//
//==========================================================================
static void showOptions (void) {
  for (int f = 0; options[f].name; ++f) {
    printf("  %s\n", options[f].name);
    /*
    switch (options[f].type) {
      case otInt: printf("%d", options[f].defi); break;
      case otStr:
      case otKey:
        printf("%s", options[f].defs);
        break;
    }
    putchar('\n');
    */
  }
}


//==========================================================================
//
//  getOptions
//
//==========================================================================
static void getOptions (int argc, char *argv[]) {
  //unsigned int numlockmask = 0;
  const char *opt;
  char *end;
  long v;

  // modifier stuff taken from evilwm
  /*
  XModifierKeymap *modmap = XGetModifierMapping(dpy);
  for (int f = 0; f < 8; f += 1) {
    for (int c = 0; c < modmap->max_keypermod; ++c) {
      if (modmap->modifiermap[f*modmap->max_keypermod+c] == XKeysymToKeycode(dpy, XK_Num_Lock)) {
        numlockmask = (1 << f);
      }
    }
  }
  XFreeModifiermap(modmap);
  */

  optWidth = DisplayWidth(dpy, screen);
  for (int f = 0; options[f].name != NULL; f += 1) {
    opt = XGetDefault(dpy, progname, options[f].name);
    switch (options[f].type) {
      case otInt:
        if (opt) {
          v = strtol(opt, &end, 10);
          if (*end == 0) {
            *(options[f].uopt.optI) = (int)v;
          } else {
            fprintf(stderr, "WARNING: option '%s' has invalid value '%s'!\n",
                    options[f].name, opt);
          }
        }
        break;
      case otStr:
        if (*(options[f].uopt.optS)) free(*(options[f].uopt.optS));
        *(options[f].uopt.optS) = strdup(opt ? opt : options[f].defs ?: "");
        break;
      case otKey:
        if (grabKey(options[f].uopt.optKS, (opt ? opt : options[f].defs)) == 0) {
          if (options[f].requiredkey) {
            fprintf(stderr, "FATAL: required keybinding cannot be set!\n");
            exit(1);
          }
        }
        break;
    }
  }
  if (optWidth < 1) optWidth = DisplayWidth(dpy, screen);
}


//==========================================================================
//
//  eventEnter
//
//==========================================================================
static void eventEnter (XEvent *e) {
  //if (prevKBGroup < 0) prevKBGroup = activeLayout(dpy);
  if (termwin != None) XSetInputFocus(dpy, termwin, RevertToPointerRoot, CurrentTime);
}


//==========================================================================
//
//  eventLeave
//
//==========================================================================
static void eventLeave (XEvent *e) {
  if (optMouseDeactivate) {
    if (lastFocused && e->xcrossing.detail != NotifyInferior) {
      XSetInputFocus(dpy, lastFocused, RevertToPointerRoot, CurrentTime);
      //if (prevKBGroup >= 0) { setActiveLayout(dpy, prevKBGroup); prevKBGroup = -1; }
    }
  }
}


//==========================================================================
//
//  toggle_window
//
//==========================================================================
static void toggle_window (void) {
  if (!hidden) {
    // we are visible, hide us
    XGetInputFocus(dpy, &currentFocused, &revert_to);
    if (currentFocused == termwin || currentFocused == win) prevMyKBGroup = activeLayout(dpy);
    if (lastFocused && termwin != None && currentFocused == termwin) {
      XSetInputFocus(dpy, lastFocused, RevertToPointerRoot, CurrentTime);
      if (prevKBGroup >= 0) setActiveLayout(dpy, prevKBGroup);
    } /*else XSetInputFocus(dpy, PointerRoot, RevertToPointerRoot, CurrentTime);*/
    prevKBGroup = -1;
    XMoveWindow(dpy, win, optX, -height-optHandleWidth);
    hidden = 1;
    //XSync(dpy, False);
  } else {
    // we are invisible, show us
    prevKBGroup = activeLayout(dpy);
    XRaiseWindow(dpy, win);
    XGetInputFocus(dpy, &lastFocused, &revert_to);
    if (termwin != None) XSetInputFocus(dpy, termwin, RevertToPointerRoot, CurrentTime);
    setActiveLayout(dpy, prevMyKBGroup);
    XMoveWindow(dpy, win, optX, 0);
    if (firstTimeH) {
      firstTimeH = 0;
      TRANSPARENCY_HACK;
    }
    hidden = 0;
  }
}


//==========================================================================
//
//  eventUnmap
//
//==========================================================================
static void eventUnmap (XEvent *e) {
  if (e->xunmap.window == termwin) {
    if (optRestart) {
      if (!hidden) {
        toggle_window();
      }
      runTerm(0);
      if (reset_restart) {
        reset_restart = 0;
        optRestart = old_restart;
      }
      XSync(dpy, False);
      //if (prevKBGroup < 0) prevKBGroup = activeLayout(dpy);
      if (termwin != None) XSetInputFocus(dpy, termwin, RevertToPointerRoot, CurrentTime);
    } else {
      if (lastFocused) {
        XSetInputFocus(dpy, lastFocused, RevertToPointerRoot, CurrentTime);
        //if (prevKBGroup >= 0) { setActiveLayout(dpy, prevKBGroup); prevKBGroup = -1; }
      }
      XSync(dpy, False);
      exit(0);
    }
  }
}


//==========================================================================
//
//  daemonize
//
//==========================================================================
static void daemonize (void) {
  pid_t pid, sid;
  if (getppid() == 1) return; // get out of here if we are DEMON
  pid = fork();

  // error?
  if (pid < 0) {
    fprintf(stderr, "ERROR: our fork is broken!\n");
    exit(1);
  }

  // parent process should die now
  if (pid > 0) {
    exit(0);
  }

  // fix file mode mask
  umask(0);

  // create new SID
  sid = setsid();
  if (sid < 0) exit(1);

  // change dir (to unlock process working dir)
  if (chdir("/") < 0) exit(1);

  // redirect standard files to /dev/null
  int nullfd = open("/dev/null", O_RDONLY);
  if (nullfd < 0) {
    fprintf(stderr, "ERROR: cannot open \"/dev/null\"!\n");
    exit(1);
    return;
  }
  freopen("/dev/null", "r", stdin);
  freopen("/dev/null", "w", stdout);
  freopen("/dev/null", "w", stderr);
  dup2(nullfd, STDIN_FILENO);
  dup2(nullfd, STDERR_FILENO);
  dup2(nullfd, STDOUT_FILENO);
  close(nullfd);

  #if 0
  freopen("/tmp/yeahdebug_out.log", "a", stdout);
  freopen("/tmp/yeahdebug_err.log", "a", stderr);
  #endif
}


//==========================================================================
//
//  main
//
//==========================================================================
int main (int argc, char *argv[]) {
  unsigned long kmods;
  KeySym key;
  XEvent event;
  /* strip the path from argv[0] if there is one */
  progname = strrchr(argv[0], '/');
  progname = progname ? progname+1 : argv[0];
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-h") == 0 || strcmp(argv[f], "--help") == 0) {
      printf(
        "%s: command cosole\n"
        "you can configure me via xresources:\n"
        "%s*foo:value\n"
        "foo can be any standard xterm/urxvt xresource or:\n"
        "resource: default value\n"
        "",
        progname, progname);
      showOptions();
      return 1;
    }
    if (strcmp(argv[f], "-D") == 0 || strcmp(argv[f], "--debug") == 0) {
      optDaemonize = 0;
      for (int c = f+1; c < argc; ++c) argv[c-1] = argv[c];
      argv[--argc] = NULL;
      --f;
    }
  }

  if (optDaemonize) daemonize();

  if (!(dpy = XOpenDisplay(NULL))) {
    fprintf(stderr, "FATAL: can not open dpy %s\n", XDisplayName(NULL));
    return 1;
  }

  screen = DefaultScreen(dpy);
  root = RootWindow(dpy, screen);
  XSetErrorHandler(handle_xerror);
  cursor = XCreateFontCursor(dpy, XC_double_arrow);

  getOptions(argc, argv);
  createWindow();

  buildTermCommand(argc, argv);
  if (!optDaemonize) {
    fprintf(stderr, "CMD: <%s>\n", command);
  }

  if (runTerm(1)) {
    fprintf(stderr, "FATAL: can't start terminal!\n");
    return 2;
  }

  for (;;) {
    XNextEvent(dpy, &event);
    switch (event.type) {
      case EnterNotify: eventEnter(&event); break;
      case LeaveNotify: eventLeave(&event); break;
      case UnmapNotify: eventUnmap(&event); break;
      case ButtonPress: resize(); break;
      /*
      case FocusIn:
      case FocusOut:
        {
          int focused = (event.type == FocusIn);
          XFocusChangeEvent *fev = (XFocusChangeEvent *)&event;
          fprintf(stderr, "focused=%d; termwin=%u; win=%u\n", focused, (unsigned)termwin, (unsigned)fev->window);
          if (fev->window == win) {
            if (focused) {
              // our window is focused, save previous layout, set our layout
              prevKBGroup = activeLayout(dpy);
              setActiveLayout(dpy, prevMyKBGroup);
              fprintf(stderr, "  1: prevKBGroup=%d; prevMyKBGroup=%d\n", prevKBGroup, prevMyKBGroup);
            } else {
              // our window is unfocused, save our layout, set previous layout
              //prevMyKBGroup = activeLayout(dpy);
              if (prevKBGroup >= 0) setActiveLayout(dpy, prevKBGroup);
              fprintf(stderr, "  0: prevKBGroup=%d; prevMyKBGroup=%d\n", prevKBGroup, prevMyKBGroup);
              prevKBGroup = -1;
            }
          }
        }
        break;
      */
      case KeyPress:
        //prevMyKBGroup = activeLayout(dpy); // save our layout here
        //key = XkbKeycodeToKeysym(dpy, event.xkey.keycode, 0, 0);
        kmods = (event.xkey.state & (Mod_Ctrl | Mod_Shift | Mod_Alt /*| Mod_AltGr*/ | Mod_Hyper));
        key = XLookupKeysym(&event.xkey, 0);
        if (key == NoSymbol) break;
        if (key == optKeyToggle.ksym && kmods == optKeyToggle.mods) {
          toggle_window();
          break;
        }
        if (key == optKeyRestartOnce.ksym && kmods == optKeyRestartOnce.mods) {
          if (optDaemonize == 0) {
            fprintf(stderr, "RESTART-ONCE activated!\n");
          }
          XBell(dpy, 100);
          if (reset_restart == 0) {
            reset_restart = 1;
            old_restart = optRestart;
          }
          optRestart = 1;
          if (hidden) toggle_window();
          break;
        }
        if (!hidden) {
          if (key == optKeyFullScreen.ksym && kmods == optKeyFullScreen.mods) {
            if (!fullscreen) {
              old_height = height;
              height = DisplayHeight(dpy, screen);
              fullscreen = 1;
            } else {
              height = old_height;
              fullscreen = 0;
            }
            firstTimeH = 1;
          } else if (key == optKeyBigger.ksym && kmods == optKeyBigger.mods) {
            height += resizeInc;
            firstTimeH = 1;
          } else if (key == optKeySmaller.ksym && kmods == optKeySmaller.mods) {
            height -= resizeInc;
            firstTimeH = 1;
          } else if (key == optKeyMouseDeactivate.ksym && kmods == optKeyMouseDeactivate.mods) {
            optMouseDeactivate = !optMouseDeactivate;
          }

          if (height < resizeInc) {
            height = resizeInc;
            firstTimeH = 1;
          }

          if (firstTimeH) {
            firstTimeH = 0;
            if (termwin != None) XResizeWindow(dpy, termwin, optWidth, height);
            XResizeWindow(dpy, win, optWidth, height+optHandleWidth);
            /*XSetInputFocus(dpy, termwin, RevertToPointerRoot, CurrentTime);*/
          }
        }
        break;
    }
  }

  return 0;
}
