/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef XKB_UTILS_H
#define XKB_UTILS_H

#include <X11/Xlib.h>


// get current layout
extern int activeLayout (Display *dpy);

// set current layout
extern void setActiveLayout (Display *dpy, int group);

// get number of available layout names
extern int layoutCount (Display *dpy);

// get available layout names
// returns counter
extern int layoutNames (Display *dpy, char *names[]);

// free names list got from layoutNames
extern void freeLayoutNames (char **names, int cnt);

// debug: print known layouts
extern void printLayouts (Display *dpy);


#endif
